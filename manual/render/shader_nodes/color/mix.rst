.. _bpy.types.ShaderNodeMixRGB:
.. DO NOT EDIT FILE. This is simply a stub which copies everything from the link below.
.. Editor's Note: This page gets copied into:
.. - :doc:`</modeling/geometry_nodes/utilities/color/mix_rgb>`
.. --- copy below this line ---

**************
Mix Color Node
**************

.. include:: /render/shader_nodes/converter/mix.rst
   :start-after: .. --- copy below this line ---
