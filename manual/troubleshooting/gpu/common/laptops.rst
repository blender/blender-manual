:orphan:

Laptops
=======

Laptops often have two GPUs for power saving purposes. One slower onboard GPU integrated into the main CPU (typically
Intel or AMD) that uses lower power, and one faster dedicated GPU for better performance (AMD or NVIDIA) that uses
more power.

For the best performance, the dedicated GPU should be used for Blender. Which GPU to use for which application can be
configured in your graphics driver or operating system settings.

If there is a graphics glitch or crash specific to the onboard GPU, then using the dedicated GPU can help avoid that.
Or vice versa, if the dedicated GPU causes issues, then using the onboard graphics can help.
