.. _gpencil_draw-toolbar-index:

*************
Drawing Tools
*************

.. figure:: /images/grease-pencil_modes_draw_tools_toolbar-tools.png
   :align: right

Cursor
   Change the location of the 3D Cursor.

:doc:`Brush </grease_pencil/modes/draw/tools/brush>`
   Tool to use for any of the :doc:`drawing brushes </grease_pencil/modes/draw/brushes/draw>`.

:ref:`Erase <tool-grease-pencil-draw-erase>`
   Erase strokes.

:ref:`Fill <tool-grease-pencil-draw-fill>`
   Automatic fill closed strokes areas.

:ref:`Box <tool-grease-pencil-draw-box>`
   Draw rectangular shapes.

   :ref:`Circle <tool-grease-pencil-draw-circle>`
      Draw oval shapes.

   :ref:`Line <tool-grease-pencil-draw-line>`
      Draw straight lines.

   :ref:`Polyline <tool-grease-pencil-draw-polyline>`
      Draw straight multiple lines.

   :ref:`Arc <tool-grease-pencil-draw-arc>`
      Draw simple arcs.

   :ref:`Curve <tool-grease-pencil-draw-curve>`
      Draw complex Bézier style curves.

:ref:`Trim <tool-grease-pencil-draw-trim>`
   Cut strokes in between others.

:ref:`Eyedropper <tool-grease-pencil-draw-eyedropper>`
   Eyedropper to create new materials or palette color based on sampled colors in the 3D Viewport.

:ref:`Interpolate <tool-grease-pencil-draw-interpolate>` :kbd:`Ctrl-E`
   Automatically create a breakdown keyframe between two normal keyframes.
