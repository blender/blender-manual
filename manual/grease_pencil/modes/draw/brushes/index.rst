
###########
  Brushes
###########

There are a number of brushes for draw mode bundled in the *Essentials* asset library. This is an
overview of all of them.


:doc:`Draw Brushes </grease_pencil/modes/draw/brushes/draw>`
============================================================

Draw brushes are the special type of brushes that uses Grease Pencil for drawing tools.
The brush can be changed in the Tool Settings.
The different draw brushes (pencil, Ink, marker, etc.) are settings variations of the same *Draw Brush*.
You can create many brushes, each with unique settings to get different artistic result while drawing.


Fill Brushes
============

Fill brushes are the special type of brushes that uses Grease Pencil for the *Fill* tools.
The brush can be changed in the Tool Settings.
The different fill brushes are settings variations of the same *Fill Brush*.
You can create many brushes, each with unique settings to get different result when filling areas.


.. _grease_pencil-draw-brushes-erase:

Erase Brushes
=============

Erase brushes are the special types of brushes that uses Grease Pencil for *Erase* tools.
The brush can be changed in the Tool Settings.
Soft and hard eraser brushes are settings variations of the same *Erase Brush*.
You can create many brushes, each with unique settings to get different effects while erasing.
The *Erase Brush* has also other two special eraser types: point and stroke.


.. toctree::
   :maxdepth: 2
   :hidden:

   draw.rst
   tint.rst
