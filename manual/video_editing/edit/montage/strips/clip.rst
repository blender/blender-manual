.. _bpy.types.MovieClipSequence:

**********
Clip Strip
**********

Clip can be modified within the :doc:`/editors/clip/index`.


Options
=======

.. reference::

   :Panel:     :menuselection:`Sidebar region --> Strip --> Movie Clip`

.. _bpy.types.MovieClipSequence.clip:

Movie Clip
   Used to select the movie clip. For controls see :ref:`ui-data-block`.

.. _bpy.types.MovieClipSequence.stabilize2d:

Use: 2D Stabilized Clip
   Use the 2D stabilized version of the clip.

.. _bpy.types.MovieClipSequence.undistort:

Use: Undistorted Clip
   Use the undistorted version of the clip.

Below the properties, shows the frame range of the movie clip (before strip adjustments).
