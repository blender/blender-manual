
#################
  Tool Settings
#################

.. toctree::
   :maxdepth: 2

   /sculpt_paint/brush/index.rst
   brush_settings.rst
   symmetry.rst
