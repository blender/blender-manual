
#########
  Tools
#########

.. toctree::
   :maxdepth: 1

   brush_tool.rst
   mask_tools.rst
   hide_tools.rst
   face_set_tools.rst
   trim_tools.rst
   line_project.rst
   mesh_filter.rst
   cloth_filter.rst
   color_filter.rst
   edit_face_set.rst
   mask_by_color.rst
   transforms.rst
