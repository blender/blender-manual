
*******
Editing
*******

Sample Color
============

.. reference::

   :Mode:      Texture Paint Mode
   :Shortcut:  :kbd:`Shift-X`

Copies the color from any part of the user interfaces and sets it as the active :ref:`Brush Color <bpy.types.Brush.color>`.

- Press :kbd:`Shift-X` to sample a color from the image and set it as the primary brush color.
- In **Texture Paint**, :kbd:`Shift-Ctrl-X` samples the **merged viewport color**, while :kbd:`Shift-X`
   samples only the currently active texture.
