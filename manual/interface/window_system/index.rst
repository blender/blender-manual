
#################
  Window System
#################

.. toctree::
   :maxdepth: 1

   Introduction <introduction.rst>
   splash.rst
   topbar.rst
   workspaces.rst
   status_bar.rst
   areas.rst
   regions.rst
   tabs_panels.rst
