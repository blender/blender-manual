
*****************
Blender's History
*****************

The Beginning
=============

Blender was created by Ton Roosendaal, a Dutch art director and self-taught software developer. Attracted to all
things technical and creative, Roosendaal began a degree in Industrial Design, but dropped out in order to start his
own 3D animation studio, NeoGeo, in 1989 (the video game console of the same name appeared a year later). Initially
based in Roosendaal's attic, NeoGeo grew rapidly, garnering awards and becoming the biggest company of its type in the
Netherlands.

Roosendaal wrote the first source files titled "Blender" on the 2nd of January, 1994, still considered Blender's
official birthday. Originally, Blender was planned as an in-house application for NeoGeo; it grew from a series of
pre-existing tools, including a ray-tracer built for the Amiga. This early version of Blender was intended to address
a perennial frustration among creatives: when a difficult client requires multiple changes to a project, how do you
implement those changes painlessly? Thanks to its highly configurable approach, Blender aimed at providing an answer.
(As an aside: the name refers to a song by a Swiss electronic band, Yello).

Roosendaal invested his savings in a Silicon Graphics workstation. Costing the equivalent of thirty thousand US
dollars, this computer led to Blender 1.0. Launched in January 1995, this first iteration of Blender proper
incorporated then innovative ideas, including a single window which could be subdivided as the user saw fit.

At the time, 3D was considered commercially uninteresting. However, Roosendaal had fallen in love with what he
describes as its "magical ability to create a whole world in a computer." So when NeoGeo closed, he and partner Frank
van Beek founded a new company focused on further developing and marketing Blender. Not a Number (NaN) opened its
doors in June 1998, distributing Blender under a freemium pricing strategy: the software was free to download, with
NaN selling keys to unlock more advanced features.

Blender Goes Open Source
========================

Thanks to this business model, NaN was able to fund a booth at a renowned computer graphics conference in Los Angeles,
SIGGRAPH (Special Interest Group on Computer Graphics and Interactive Techniques). As a consequence, Blender attracted
two rounds of funding totaling some five and a half million US dollars. Despite this investment, a harsh economic
climate, excess spending, and troubled relations between NaN and its investors meant that the company closed in early
2002.

With NaN's demise, Blender's development ceased. Unable to buy the rights from NaN's backers, Roosendaal opted for a
novel plan. In May of 2002, he started a non-profit, the Blender Foundation, with the intention of making Blender
`open-source <https://opensource.org/>`__. His hope was to create a public monument to Blender, and give everyone who
had worked on the Blender project the chance to use it for their portfolios. In July of the same year, he launched the
first-ever crowdfunding campaign: Free Blender. Thanks to Blender's community of 250,000 users, the Blender Foundation
was able to raise one hundred and ten thousand euros in just seven weeks — sufficient to regain Blender from its
investors.

On Sunday, October 13th, 2002, Blender was released under the terms of the `GNU General Public License
<https://www.gnu.org/licenses/gpl.html>`__, the strictest possible open-source contract. Not only would Blender be
free, but its source code would remain free, forever, to be used for any purpose whatsoever.

The success of Free Blender cleared the way for a style of development that has become Blender's defining strength.
While Blender's evolution is partly driven by grant-funded developers and guided by a core team at the Blender
Foundation, Amsterdam, its greatest advantage is a global community of dedicated volunteers. Thanks to their efforts,
Blender is able to iterate rapidly and respond to the needs of artists and makers. Such nimbleness and creativity
would be much harder within the confines of a traditional business model.

Blender Makes Open Movies
=========================

As a way to stress-test Blender's increasing power, the Blender Foundation challenged its community's most talented
artists to make an animated 3D short film. The only criterion was that they had to use open source tools, with Blender
prime among them.

Under the codename "Project Orange," this project began in 2005, resulting in Elephants Dream, a surreal adventure
through a gigantic machine. The film and all its assets were made freely available under a Creative Commons license.

After the success of Elephants Dream, the Blender Institute was established in the summer of 2007. As well as helping
to define the Blender Foundation's goals, the Blender Institute comprised a permanent office and studio, with the
express intention of generating Open Projects related to 3D movies, games or visual effects. As part of its output,
the Blender Institute has created a series of Open Movies in collaboration with leading artists. They include the
comedy Big Buck Bunny (2008), science fiction thriller Tears of Steel (2012), a poetic fantasy Spring (2019), and
horror-comedy Sprite Fright (2021).

Blender Landmarks
=================

Each Open Project places new demands on Blender as a 3D creation suite, which in turn leads to further upgrades. While
a complete list of updates is beyond the scope of this article, some milestones are worth noting.

Early 2008 saw the start of the Blender 2.5 project. This combined a major User Interface overhaul, with new tool
definitions, a data access system, event handling, and a new animation system. For 2.5, the primary goal was to bring
the interface standards and input methods up to date.

Cycles is Blender's production-capable path-tracing render engine, first incorporated into release 2.61, back in 2011.
Over the years, Cycles has introduced support for a wide range of rendering possibilities, including AMD and NVIDIA.
Similarly, it's grown to include support for many features including hair, motion blur, smoke and fire, major shaders
and materials, adaptive subdivisions, and much more.

With its watershed 2.8 release in July, 2019, Blender broke into the 3D mainstream. Starting with a drastically
revamped User Interface, the 2.8 series included a multitude of innovations, from EEVEE (a real-time render engine),
to new remeshing options for sculptors, to the integration of Mantaflow, to a fully functioning 2D animation workspace
that also offered the possibility of a 2D/3D hybrid workflow.

Although industry recognition for Blender had grown over the decades, 2.8 marked the moment when it was widely
accepted as a legitimate alternative to paid competitors. As well as using Blender in their own projects, some of the
world's largest and most recognized companies became regular contributors to the Blender Development Fund, ensuring
that Blender can continue to innovate.

As well as Blender and Open Projects made with Blender, there's Blender Cloud. This subscription-based Open Production
platform provides rolling updates on current Open Movie projects, as well as an archive of film assets in .blend file
form, animation and shot breakdowns, shaders and textures, and comprehensive training videos from professional artists
and developers, often those employed at Blender HQ in Amsterdam.

Blender: Present And Future
===========================

In total, the Blender organization numbers some twenty-eight employees, working from Amsterdam, remotely, and on a
grant basis. For Blender, this team represents only a small part of a much wider community, which it defines as
everyone who contributes to Blender's development, earns their living from Blender, or simply downloads it.

The Blender mission can be summed up as "get the world's best 3d technology in the hands of artists as open-source,
and make amazing things with it."

Going forward, Blender hopes to become a sustainable, future proof organization, dedicated to furthering its
open-source philosophy, its values of curiosity and innovation, a commitment to technical excellence, and increasingly
ambitious creative goals.


Version/Revision Milestones
===========================

.. rubric:: The start!

- **1.00 -- January 1994:** Blender
  `in development <https://code.blender.org/2013/12/how-blender-started-twenty-years-ago/>`__
  at animation studio NeoGeo.
- **1.23 -- January 1998:** SGI version published on the web, IrisGL.
- **1.30 -- April 1998:** Linux and FreeBSD version, port to OpenGL and X11.
- **1.3x -- June 1998:** NaN founded.
- **1.4x -- September 1998:** Sun and Linux Alpha version released.
- **1.50 -- November 1998:** First Manual published.
- **1.60 -- April 1999:** C-key (new features behind a lock, $95), Windows version released.
- **1.6x -- June 1999:** BeOS and PPC version released.
- **1.80 -- June 2000:** End of C-key, Blender full freeware again.
- **2.00 -- August 2000:** Interactive 3D and real-time engine.
- **2.10 -- December 2000:** New engine, physics, and Python.
- **2.20 -- August 2001:** Character animation system.
- **2.21 -- October 2001:** Blender Publisher launch.
- **2.2x -- December 2001:** macOS version.


.. rubric:: Blender goes Open Source

13 October 2002:
   **Blender goes Open Source, 1st Blender Conference.**
2.25 -- October 2002:
   `Blender Publisher <https://download.blender.org/release/Publisher2.25/>`__ becomes freely available,
   and the experimental tree of Blender is created, a coder's playground.
2.26 -- February 2003:
   The first truly open source Blender release.
2.27 -- May 2003:
   The second open source Blender release.
2.28x -- July 2003:
   First of the 2.28x series.
`2.30 <https://archive.blender.org/www/development/release-logs/blender-230/>`__ -- October 2003:
   Preview release of the 2.3x UI makeover presented at the 2nd Blender Conference.
`2.31 <https://archive.blender.org/www/development/release-logs/blender-231/>`__ -- December 2003:
   Upgrade to stable 2.3x UI project.
`2.32 <https://archive.blender.org/www/development/release-logs/blender-232/>`__ -- January 2004:
   A major overhaul of internal rendering capabilities.
`2.33 <https://archive.blender.org/www/development/release-logs/blender-233/>`__ -- April 2004:
   Game Engine returns, ambient occlusion, new procedural textures.
`2.34 <https://archive.blender.org/www/development/release-logs/blender-234/>`__ -- August 2004:
   Particle interactions, LSCM UV mapping, functional YafRay integration, weighted creases in subdivision surfaces,
   ramp shaders, full OSA, and many (many) more.
`2.35 <https://archive.blender.org/www/development/release-logs/blender-235a/>`__ -- November 2004:
   Another version full of improvements: object hooks, curve deforms and curve tapers,
   particle duplicators and much more.
`2.36 <https://archive.blender.org/www/development/release-logs/blender-236/>`__ -- December 2004:
   A stabilization version, much work behind the scenes, normal and displacement mapping improvements.
`2.37 <https://archive.blender.org/www/development/release-logs/blender-237a/>`__ -- June 2005:
   Transformation tools and widgets, soft bodies, force fields, deflections,
   incremental subdivision surfaces, transparent shadows, and multi-threaded rendering.
`2.40 <https://archive.blender.org/wiki/2015/index.php/Dev:Ref/Release_Notes/2.40>`__ -- December 2005:
   Full rework of armature system, shape keys, fur with particles, fluids, and rigid bodies.
`2.41 <https://archive.blender.org/wiki/2015/index.php/Dev:Ref/Release_Notes/2.41>`__ -- January 2006:
   Lots of fixes, and some Game Engine features.
`2.42 <https://archive.blender.org/wiki/2015/index.php/Dev:Ref/Release_Notes/2.42>`__ -- July 2006:
   The nodes release, Array modifier, vector blur, new physics engine, rendering, lip sync, and many other features.
   This was the release following `Project Orange <https://orange.blender.org/>`__.
`2.43 <https://archive.blender.org/wiki/2015/index.php/Dev:Ref/Release_Notes/2.43>`__ -- February 2007:
   Multiresolution meshes, multi-layer UV textures, multi-layer images and multi-pass rendering and baking, sculpting,
   retopology, multiple additional mattes, distort and filter nodes, modeling and animation improvements, better
   painting with multiple brushes, fluid particles, proxy objects, Sequencer rewrite, and post-production UV
   texturing.
`2.44 <https://archive.blender.org/www/development/release-logs/blender-244/index.html>`__ -- May 2007:
   The big news, in addition to two new modifiers and re-awakening the 64-bit OS support, was the addition of
   subsurface scattering, which simulates light scattering beneath the surface of organic and soft objects.
`2.45 <https://archive.blender.org/www/development/release-logs/blender-245/index.html>`__ -- September 2007:
   Serious bug fixes, with some performance issues addressed.
`2.46 <https://archive.blender.org/wiki/2015/index.php/Dev:Ref/Release_Notes/2.46>`__ -- May 2008:
   The Peach release was the result of a huge effort of over 70 developers providing enhancements to provide hair and
   fur, a new particle system, enhanced image browsing, cloth, a seamless and non-intrusive physics cache, rendering
   improvements in reflections, AO, and render baking, a Mesh Deform modifier for muscles and such, better animation
   support via armature tools and drawing, skinning, constraints and a colorful Action Editor, and much more. It
   contained the results of `Project Peach <https://peach.blender.org/>`__.
`2.47 <https://archive.blender.org/wiki/2015/index.php/Dev:Ref/Release_Notes/2.47>`__ -- August 2008:
   Bugfix release.
`2.48 <https://archive.blender.org/www/development/release-logs/blender-248/index.html>`__ -- October 2008:
   The Apricot release, cool GLSL shaders, lights and GE improvements, snap, sky simulator, Shrinkwrap modifier, and
   Python editing improvements. This contained the results of `Project Apricot <https://apricot.blender.org/>`__.
`2.49 <https://archive.blender.org/wiki/2015/index.php/Dev:Ref/Release_Notes/2.49>`__ -- June 2009:
   Node-based textures, armature sketching (called Etch-a-Ton), Boolean mesh operation improvements, JPEG2000 support,
   projection painting for direct transfer of images to models, and a significant Python script catalog. GE
   enhancements included video textures, where you can play movies in-game, upgrades to the Bullet physics engine,
   dome (fisheye) rendering, and more API GE calls made available.


.. rubric:: Blender 2.5x -- The Recode!

`2.5x <https://www.blender.org/download/releases/#25-series-2009-2011>`__ -- From 2009 to August 2011:
   This series released four `pre-version
   <https://archive.blender.org/www/development/release-logs/blender-256-beta>`__ (from Alpha 0 in November 2009 to
   Beta in July 2010) and three stable versions (from 2.57 - April 2011 to 2.59 - August 2011). It was one of the most
   important development projects, with a total refactor of the software with new functions, redesign of the internal
   window manager and event/tool/data handling system, and new Python API. The final version of this project was
   Blender 2.59 in August 2011.


.. rubric:: Blender 2.6x to 2.7x -- Improvements & Stabilizing

`2.60 <https://archive.blender.org/wiki/2015/index.php/Dev:Ref/Release_Notes/2.60>`__ -- October 2011:
   Internationalization of the UI, improvements in the animation system and the GE, vertex weight groups modifiers, 3D
   audio and video, and bug fixes.
`2.61 <https://archive.blender.org/wiki/2015/index.php/Dev:Ref/Release_Notes/2.61>`__ -- December 2011:
   The Cycles renderer was added to the trunk, the camera tracker was added, dynamic paint for modifying textures with
   mesh contact/approximation, the Ocean modifier to simulate ocean and foam, new add-ons, bug fixes, and more
   extensions added for the Python API.
`2.62 <https://archive.blender.org/wiki/2015/index.php/Dev:Ref/Release_Notes/2.62>`__ -- February 2012:
   The `Carve library <https://code.google.com/archive/p/carve/>`__ was added to improve Boolean operations, support
   for object tracking was added, the Remesh modifier was added, many improvements in the GE, matrices and vectors in
   the Python API were improved, plus new add-ons, and many bug fixes.
`2.63 <https://archive.blender.org/wiki/2015/index.php/Dev:Ref/Release_Notes/2.63>`__ -- April 2012:
   Bmesh was merged with the trunk, with full support for n-sided polygons, sculpt hiding, a panoramic camera for
   Cycles, mirror ball environment textures and float precision textures, render layer mask layers, ambient occlusion
   and viewport display of background images and render layers. New import and export add-ons were added, and 150 bug
   fixes.
`2.64 <https://archive.blender.org/wiki/2015/index.php/Dev:Ref/Release_Notes/2.64>`__ -- October 2012:
   A mask editor was added, along with an improved motion tracker, OpenColorIO, Cycles improvements, Sequencer
   improvements, better mesh tools (Inset and Bevel were improved), new keying nodes, sculpt masking, Collada
   improvements, a new Skin modifier, a new compositing nodes backend, and the fixing of many bugs.
`2.65 <https://archive.blender.org/wiki/2015/index.php/Dev:Ref/Release_Notes/2.65>`__ -- December 2012:
   Fire and smoke improvements, anisotropic shader for Cycles, modifier improvements, the Bevel tool now includes
   rounding, new add-ons, and over 200 bug fixes.
`2.66 <https://www.blender.org/download/releases/2-66>`__ -- February 2013:
   Dynamic topology, rigid body simulation, improvements in UI and usability (including retina display support),
   Cycles now supports hair, the Bevel tool now supports individual vertex beveling, new :doc:`Mesh Cache
   </modeling/modifiers/modify/mesh_cache>` modifier and the new :doc:`UV Warp </modeling/modifiers/modify/uv_warp>`
   modifier, new SPH particle fluid solver. More than 250 bug fixes.
`2.67 <https://www.blender.org/download/releases/2-67>`__ -- May 2013:
   Freestyle was added, paint system improvements, subsurface scattering for Cycles, Ceres library in the motion
   tracker, new custom Python nodes, new mesh modeling tools, better support for UTF-8 text and improvements in Text
   editors, new add-ons for 3D printing, over 260 bug fixes.
`2.68 <https://www.blender.org/download/releases/2-68>`__ -- July 2013:
   New and improved modeling tools, three new Cycles nodes, big improvements in the motion tracker, Python scripts and
   drivers are disabled by default when loading files for security reasons, and over 280 bug fixes.
`2.69 <https://www.blender.org/download/releases/2-69>`__ -- October 2013:
   Even more modeling tools, Cycles improved in many areas, plane tracking is added to the motion tracker, better
   support for FBX import/export, and over 270 bugs fixed.
`2.70 <https://www.blender.org/download/releases/2-70>`__ -- March 2014:
   Cycles gets basic volumetric support on the CPU, more improvements to the motion tracker, two new modeling
   modifiers, some UI consistency improvements, and more than 560 bug fixes.
`2.71 <https://www.blender.org/download/releases/2-71>`__ -- June 2014:
   Deformation motion blur and fire/smoke support is added to Cycles, UI pop-ups are now draggable. There are
   performance optimizations for sculpting mode, new interpolation types for animation, many improvements to the GE,
   and over 400 bug fixes.
`2.72 <https://www.blender.org/download/releases/2-72>`__ -- October 2014:
   Cycles gets volume and SSS support on the GPU, pie menus are added and tooltips greatly improved, the Intersection
   modeling tool is added, new Sun Beam node for the Compositor, Freestyle now works with Cycles, texture painting
   workflow is improved, and more than 220 bug fixes.
`2.73 <https://www.blender.org/download/releases/2-73/>`__ -- January 2015:
   Cycles gets improved volumetric support, major upgrade to Grease Pencil, Windows gets Input Method Editors (IMEs)
   and general improvements to painting, Freestyle, Sequencer and add-ons.
`2.74 <https://www.blender.org/download/releases/2-74/>`__ -- March 2015:
   Support for custom normals, viewport compositing and improvements to hair dynamics.
`2.75 <https://www.blender.org/download/releases/2-75/>`__ -- July 2015:
   Integrated stereo/multi-view pipeline, Smooth Corrective modifier and new developmental dependency graph.
`2.76 <https://www.blender.org/download/releases/2-76/>`__ -- November 2015:
   Pixar OpenSubdiv support, Viewport and File Browser performance boost,
   node auto-offset, and a text effect strip for the Sequencer.
`2.77 <https://www.blender.org/download/releases/2-77/>`__ -- March 2016:
   OpenVDB support for caching of smoke/volumetric simulations, improved Cycles subsurface scattering, Grease Pencil
   stroke sculpting and improved workflow, and reworked library handling to manage missing and deleted data-blocks.
`2.78 <https://www.blender.org/download/releases/2-78/>`__ -- September 2016:
   Cycles support for spherical stereo images for VR, Grease Pencil works more similar to other 2D drawing software,
   Alembic import and export support, and improvements to Bendy Bones for easier and simpler rigging.
`2.79 <https://www.blender.org/download/releases/2-79/>`__ -- September 2017:
   New Cycles features: Denoising, Shadow catcher, and new Principled shader. Other improvements were made to Grease
   Pencil and Alembic. Support was also added for application templates.


.. rubric:: Blender 2.8 -- Revamped UI

`2.80 <https://www.blender.org/download/releases/2-80>`__ -- July 2019:
   A totally redesigned UI for easier navigation; improved viewport, gizmos, and tools. With EEVEE a new physically
   based real-time render engine was created. The Grease Pencil got a big overhaul and is now a full 2D drawing and
   animation system. Replacing the old layers, collections are a powerful way to organize objects. Other improvements:
   Cycles, Modeling, Animation, Import/Export, Dependency Graph.
`2.81 <https://www.blender.org/download/releases/2-81/>`__ -- November 2019:
   Revamped sculpting tools, Cycles OptiX accelerated rendering, denoising, many EEVEE improvements, library
   overrides, UI improvements and much more.
`2.82 <https://www.blender.org/download/releases/2-82/>`__ -- February 2020:
   UDIM and USD support, Mantaflow for fluids and smoke simulation, AI denoising, Grease Pencil improvements, and much
   more.
`2.83 LTS <https://www.blender.org/download/releases/2-83/>`__ -- June 2020:
   3D Viewport virtual reality scene inspection, new volume object type, Cycles adaptive sampling, Cycles viewport
   denoising, sculpting improvements, and much more. **First LTS release intended to support studio and long lifecycle
   project use.**


.. rubric:: Blender 2.9 -- Refining 2.8

`2.90 <https://www.blender.org/download/releases/2-90/>`__ -- August 2020:
   Improved sky texture, EEVEE motion blur, sculpting improvements, revamped modifier UI, improved modeling tools, and
   faster motion blur in Cycles.
`2.91 <https://www.blender.org/download/releases/2-91/>`__ -- November 2020:
   Outliner improvements, property search, improved mesh Boolean operations, animation curves, volume object and
   display improvements, and more refined sculpting tools.
`2.92 <https://www.blender.org/download/releases/2-92/>`__ -- February 2021:
   Geometry nodes, primitive add tool, sculpting improvements, Grease Pencil curve editing, Cycles Color Attribute
   baking, APIC fluid simulations, Video Sequencer improvements, and much more.
`2.93 LTS <https://www.blender.org/download/releases/2-93/>`__ -- June 2021:
   New geometry nodes, sculpting improvements, Grease Pencil Line Art modifier along with other improvements, an
   improved :abbr:`DOF (Depth Of Field)` for the EEVEE render engine, redesigned Cryptomatte workflow, and more. **LTS
   release for the 2.9 series.**


.. rubric:: Blender 3.0 -- Optimizing Performance

`3.0 <https://www.blender.org/download/releases/3-0/>`__ -- December 2021
   Asset Browser added, Cycles X, EEVEE Attributes, New geometry nodes, animation update, Grease Pencil Line Art
   improvements, pose library, Open Image Denoising 2-8x faster, additional support for AMD on linux.

`3.1 <https://www.blender.org/download/releases/3-1/>`__ -- March 2022
   Major point clouds improvements, Cycles Apple Metal GPU support, Subdivision GPU support, image editor handles
   larger images, Major performance gains for geometry nodes, context aware search for geometry nodes.

`3.2 <https://www.blender.org/download/releases/3-2/>`__ -- June 2022
   Light groups for Cycles, true Shadow caustics, volume motion blur, GLTF improvements, AMD GPU Rendering on Linux,
   painting in sculpt mode, WEBp image support.

`3.3 LTS <https://www.blender.org/download/releases/3-3/>`__ -- September 2022
   New hair object, procedural UV nodes, Line Art shadow and contour, Intel GPU rendering support via oneAPI, and
   improvements to library overrides. **First LTS release of the 3.0 series.**

`3.4 <https://www.blender.org/download/releases/3-4/>`__ -- December 2022
   Cycles path guiding, sculpting auto masking improvements, even more geometry nodes, UV Editing improvements and
   Wayland support on Linux.

`3.5 <https://www.blender.org/download/releases/3-5/>`__ -- March 2023
   New generative hair assets, vector displacement maps for sculpting, viewport compositor, and Cycle's light trees.

`3.6 LTS <https://www.blender.org/download/releases/3-6/>`__ -- June 2023
   Simulation nodes added to Geometry Nodes, Cycles hardware ray-tracing for AMD and Intel, UV island packing, asset
   bundle from Blender Studio and community artists included, new retopology overlay. **Final LTS of the 3.0 series.**

.. rubric:: Blender 4.0 -- A Major Leap For Rendering, Creating Tools, and More

`4.0 <https://www.blender.org/download/releases/4-0/>`__ -- November 2023
   A new Principled BSDF shader with coat and sheen layers, AgX view transform, Voronoi Texture fractal noise, light
   linking for selective lighting, run Geometry Nodes as Node Tools, snapping improvements including Snap Base, menu
   and modifier type-to-search, new `Inter <https://rsms.me/inter/>`__ typeface, streamlined keymap, bone collections,
   Hydra Storm USD renderer, larger asset library, alignment to the `VFX Reference Platform 2023
   <https://vfxplatform.com/>`__.

`4.1 <https://www.blender.org/download/releases/4-1/>`__ -- March 2024
   Geometry Nodes baking support, Menu Switch node, OpenImageDenoise GPU acceleration, more realtime viewport
   compositor functions, simpler animation keyframe insertion, hierarchical bone collections, graph editor
   click-and-slide, video sequencer performance and color scope improvements, alignment to the `VFX Reference Platform
   2024 <https://vfxplatform.com/>`__, armature and shape key export to USD.

`4.2 LTS <https://www.blender.org/download/releases/4-2/>`__ -- July 2024
   Next generation of EEVEE with major upgrades to lighting, sun lights, displacement, subsurface, volumetrics, and
   motion blur, Cycles gains Ray Portal BSDF and Thin-Film Interference, better soft volume rendering with reduced
   noise, blue noise-based sampling, Blender Extensions platform launched, Khronos PBR Neutral Tone Mapper, sculpting
   selection improvements, Node inputs support matrices, Node Tools can use mouse position and viewport, video
   sequencer graphical overhaul, additional USD export options, native portable installation support.
   **First LTS of the 4.0 series.**

`4.3 <https://www.blender.org/download/releases/4-3/>`__ -- November 2024
   Light linking and shadow linking in EEVEE, Metallic BSDF, Gabor noise texture, EEVEE render passes in the
   compositor, minimum stretch (SLIM) UV unwrapping, numerous Geometry Nodes updates including for...each zone,
   physics nodes, Grease Pencil engine rewritten for speed and features, over 100 default brushes now included for
   painting and sculpting, UI area docking.
