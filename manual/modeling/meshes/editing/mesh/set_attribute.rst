.. _bpy.ops.mesh.attribute_set:

*************
Set Attribute
*************

.. reference::

   :Mode:      Edit Mode
   :Menu:      :menuselection:`Mesh --> Set Attribute`

Opens a pop-up window showing the name of the :term:`active` :term:`attribute`
as well as the value of that attribute for the active element (vertex/edge/face).
From there, you can change the value to apply it to all selected elements.

The "active" attribute is the one last selected in the
:doc:`Data </modeling/meshes/properties/object_data>` tab of the
:doc:`/editors/properties_editor` editor. It could be
a :ref:`UV Map <uv-maps-panel>`,
a :ref:`Color Attribute <modeling-meshes-properties-object_data-color-attributes>`,
or a generic :doc:`Attribute </modeling/geometry_nodes/attributes_reference>`.

.. seealso::

   Attribute values can be viewed in the :doc:`/editors/spreadsheet` editor.
