.. index:: Geometry Nodes; Transpose Matrix
.. _bpy.types.FunctionNodeTransposeMatrix:

*********************
Transpose Matrix Node
*********************

.. figure:: /images/node-types_FunctionNodeTransposeMatrix.webp
   :align: right
   :alt: Transpose Matrix node.

The *Transpose Matrix* node flips a matrix over its diagonal.

.. seealso::

   `Transpose <https://en.wikipedia.org/wiki/Transpose>`__ on Wikipedia.


Inputs
======

Matrix
   The matrix to be transposed.


Properties
==========

This node has no properties.


Outputs
=======

Matrix
   The transposed matrix.
