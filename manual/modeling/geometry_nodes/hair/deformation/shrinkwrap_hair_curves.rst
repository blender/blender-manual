.. index:: Geometry Nodes; Shrinkwrap Hair Curves

**********************
Shrinkwrap Hair Curves
**********************

Shrinkwraps hair curves to a mesh surface from below and optionally from above.

.. peertube:: rA2Ytunkm2SorWhXeVQTWq


Inputs
======

Geometry
   Input Geometry (only curves will be affected).

Surface
   Surface geometry used for shrinkwrap. This input takes priority over the corresponding object input, if used.

Surface
   Surface object used for shrinkwrap.

Factor

Offset Distance
   Distance from the surface used for shrinkwrap.

Above Surface
   Blend shrinkwrap for points above the surface.

Smoothing Steps
   Amount of steps of smoothing applied after shrinkwrap.

Lock Roots
   Lock the position of root points.


Properties
==========

This node has no properties.


Outputs
=======

**Geometry**
