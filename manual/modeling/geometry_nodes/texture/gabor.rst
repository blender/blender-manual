.. index:: Geometry Nodes; Gabor Texture
.. include:: /render/shader_nodes/textures/gabor.rst
   :start-after: .. section-title
   :end-before: .. section-inputs

.. note::

   This node is ported from shader nodes. The manual and images are
   referencing the shader version of the node.
   This node accepts field inputs and outputs.
   When not connected the Vector input has an implicit ``position`` attribute value.

.. include:: /render/shader_nodes/textures/gabor.rst
   :start-after: .. section-inputs
