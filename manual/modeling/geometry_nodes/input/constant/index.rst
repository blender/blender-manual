
########################
  Input Constant Nodes
########################

Nodes used mainly as input to other nodes.

.. toctree::
   :maxdepth: 1

   boolean.rst
   collection.rst
   color.rst
   image.rst
   integer.rst
   material.rst
   object.rst
   rotation.rst
   string.rst
   value.rst
   vector.rst
