
###############
  Input Nodes
###############

Nodes used mainly as input to other nodes.

.. toctree::
   :maxdepth: 1

   Constant <constant/index.rst>
   Gizmo <gizmo/index.rst>
   Scene <scene/index.rst>
