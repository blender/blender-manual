
##############
  Mask Nodes
##############

.. toctree::
   :maxdepth: 1

   cryptomatte.rst
   cryptomatte_legacy.rst

----------

.. toctree::
   :maxdepth: 1

   box_mask.rst
   ellipse_mask.rst

----------

.. toctree::
   :maxdepth: 1

   double_edge_mask.rst
   id_mask.rst
