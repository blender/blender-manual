
**********************
Manage UI Translations
**********************

.. todo:: Add this information.


Enabling Add-on
===============

#. Open Blender and go to :doc:`/editors/preferences/addons` section of the :doc:`/editors/preferences/index`.
#. Search "Manage UI Translations" and check the *Enable Extension* checkbox.


Description
===========

See `Blender translation guide
<https://developer.blender.org/docs/handbook/translating/translator_guide/#manage-ui-translations-add-on>`__
in the Developer Handbook.

.. reference::

   :Category: System
   :Description: Allows managing UI translations directly from within Blender
                 (update main po-files, update scripts' translations, etc.).
   :Location: :menuselection:`Topbar --> File menu`, Text editor, any UI control
   :File: ui_translate folder
   :Author: Bastien Montagne
   :Note: This add-on is bundled with Blender.
