
***********************
Documentation Todo List
***********************

This page provides a list of changes that need to be made to the manual. This is a great place for new contributors to
start. This page is auto-generated from any items marked with the ``.. todo::`` tag.

It is recommended to also check the `documentation workboard
<https://projects.blender.org/blender/documentation/projects>`__ since this will contain larger initiatives and
structured work efforts, as well as laying out priorities.

.. todolist::
